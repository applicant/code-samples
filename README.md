This project is a career profile for Waheed Brown. It contains the following:

- [Resume](waheed_brown_resume.pdf)
- [Code Samples](olg.tar.gz)
- [References](references.txt)
- Written Works
	- [Seller Strategies for Virtual Auctions Using Real Currencies](thesis_WAHEED_BROWN.pdf)
	- [Bitland](http://www.amazon.com/Bitland-W-Brown-ebook/dp/B00KWEYHOE/ref=sr_1_1?ie=UTF8&qid=1441591878&sr=8-1&keywords=bitland&pebp=1441591880182&perid=01M9D3S40GAD4Q6C2A78) (a novel)
- [Awards](http://www.abic.bm/education/awards-recipients-12.html) (ctrl-f search for "Waheed", or just scroll to bottom of page)
